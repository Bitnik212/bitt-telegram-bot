start-polling:
	venv/bin/python main.py --type=polling

start-webhook:
	venv/bin/python main.py --type=webhook
