
def required_property(func):

    def decorator(self):
        property_value = func(self)
        is_valid = True
        if type(property_value) is str:
            property_str = str(property_value)
            if property_str != "":
                return property_value
            else:
                is_valid = False
        elif property_value is None:
            is_valid = False

        if not is_valid:
            raise Exception(f"Property {self.__class__.__name__}.{func.__name__} is required")
        return property_value

    return decorator
