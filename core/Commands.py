import types
from pathlib import Path

from core.Command import Command
from core import ROOT_FOLDER
from core.decorator.register_command import TRIGGER_TEXT


class Commands:

    SERVICE_FILES = ["__init__", "__pycache__"]

    def __init__(self, commands_root_folder: str):
        self.__commands_root_folder = commands_root_folder

    def __call__(self, *args, **kwargs) -> list[id(Command)]:
        commands = []
        for file in self.folder_path.iterdir():
            file_name = file.name.split(".")[0]
            if self.__is_valid_command_file_name(file_name):
                command_class = self.__get_command_class(file_name)
                if isinstance(command_class, types.FunctionType) and command_class.__name__ == TRIGGER_TEXT:
                    commands.append(command_class())
        return commands

    @staticmethod
    def __remove_file_extension(file_name: str) -> str:
        return file_name.split(".")[0]

    @staticmethod
    def __import_module(module_path: str):
        """
        Получаем модуль с командой для бота
        """
        return __import__(
            name=module_path,
            globals=globals(),
            locals=locals(),
            fromlist=["object"]
        )

    @staticmethod
    def __is_valid_command_file_name(file_name: str) -> bool:
        return file_name.find("Command") != -1 and file_name.replace("Command", "") != ""

    @property
    def folder_path(self) -> Path:
        commands_folder_name = self.__commands_root_folder
        commands_path = ROOT_FOLDER / commands_folder_name.replace(".", "/")
        if Path.exists(commands_path):
            return commands_path
        else:
            raise FileNotFoundError("Error on find commands folder path")

    def __get_command_class(self, class_name: str):
        """
        Получаем класс из модуля
        """
        module = self.__import_module(self.__get_module_path(class_name))
        return getattr(module, class_name)

    def __get_module_path(self, module_name: str) -> str:
        module_name = self.__remove_file_extension(module_name)
        path = str(self.folder_path).replace(str(ROOT_FOLDER) + "/", "")
        path = path.replace("/", ".")
        return f"{path}.{module_name}"


def get_commands(commands_root_folder: str):
    return Commands(commands_root_folder)()
