from config.Config import Config
from core.decorator.required import required_property
from urllib.parse import urlparse, ParseResultBytes, ParseResult


class WebHookConfig(Config):

    PREFIX = "WEBHOOK_"

    @property
    @required_property
    def url(self) -> ParseResultBytes | ParseResult:
        return urlparse(self.get(f'{self.PREFIX}URL', str))

    def __str__(self) -> str:
        return f"WebHookConfig(" \
               f"url={self.url}" \
               f")"
