import os
from typing import Type, Any

import dotenv
from dotenv import dotenv_values

from config import ROOT_FOLDER


class Config:

    PREFIX = ""

    def __init__(self):
        self.ENV_FILENAME = ".env"
        self.env_file_path = ROOT_FOLDER / self.ENV_FILENAME
        self.__env_file = dotenv_values(self.env_file_path)

    def get(self, property_name: str, property_type: Type, default_value: Any = None):
        property_value = self.__env_file.get(property_name, None)
        property_value = property_value if property_value is not None else os.environ.get(property_name, None)
        return property_type(property_value) if property_value is not None else default_value

    @property
    def debug(self) -> bool:
        raw_string = self.get("DEBUG", str, "")
        return raw_string.lower() == "true"
