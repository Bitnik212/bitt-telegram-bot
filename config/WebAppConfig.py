from config.Config import Config
from core.decorator.required import required_property


class WebAppConfig(Config):

    PREFIX = "WEBAPP_"

    @property
    def port(self) -> int:
        return self.get(f"{self.PREFIX}PORT", int, 3000)

    @property
    def host(self) -> str:
        return self.get(f'{self.PREFIX}HOST', str, "0.0.0.0")

    def __str__(self) -> str:
        return f"WebAppConfig(" \
               f"port={self.port}, " \
               f"host={self.host}" \
               f")"
