from config.Config import Config
from core.decorator.required import required_property


class TelegramConfig(Config):

    PREFIX = "TELEGRAM_"

    @property
    @required_property
    def token(self) -> str:
        return self.get(f"{self.PREFIX}TOKEN", str)

    @property
    def inline_query_cache_time(self) -> int:
        return self.get(f"{self.PREFIX}INLINE_QUERY_CACHE_TIME", int, 500)

    def __str__(self) -> str:
        return f"TelegramConfig(" \
               f"token={self.token}, " \
               f"inline_query_cache_time={self.inline_query_cache_time}" \
               f")"
