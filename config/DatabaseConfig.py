from config.Config import Config
from core.decorator.required import required_property


class DatabaseConfig(Config):

    PREFIX = "DATABASE_"

    @property
    @required_property
    def filename(self) -> str:
        return self.get(f"{self.PREFIX}FILENAME", str)

    @property
    @required_property
    def url(self) -> str:
        return self.get(f"{self.PREFIX}URL", str)

    def __str__(self) -> str:
        return f"DatabaseConfig(" \
               f"filename={self.filename}, " \
               f"url={self.url}" \
               f")"
