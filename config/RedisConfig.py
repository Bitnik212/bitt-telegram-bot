from config.Config import Config
from core.decorator.required import required_property


class RedisConfig(Config):

    PREFIX = "REDIS_"

    @property
    @required_property
    def host(self) -> str:
        return self.get(f"{self.PREFIX}HOST", str)

    @property
    def port(self) -> int:
        return self.get(f"{self.PREFIX}PORT", int, 6379)

    @property
    def db(self) -> int:
        return self.get(f"{self.PREFIX}DB_NUMBER", int, 5)

    @property
    def pool_size(self) -> int:
        return self.get(f"{self.PREFIX}POOL_SIZE", int, 10)

    @property
    def prefix(self) -> str:
        return self.get(f"{self.PREFIX}PREFIX", str, "bot")

    def __str__(self) -> str:
        return f"RedisConfig(" \
               f"host={self.host}, " \
               f"port={self.port}, " \
               f"db={self.db}, " \
               f"pool_size={self.pool_size}, " \
               f"prefix={self.prefix}" \
               ")"
