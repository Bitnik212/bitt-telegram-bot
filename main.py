import sys

from aiogram.utils import executor
from aiogram.utils.executor import start_webhook

from app.Bot import TelegramBot
from config.WebAppConfig import WebAppConfig
from config.WebHookConfig import WebHookConfig

webhook_config = WebHookConfig()
webapp_config = WebAppConfig()


def main(*args):
    bot = TelegramBot()
    for arg in list(args)[0]:
        if str(arg).startswith("--"):
            raw_arg = str(arg).replace("--", "").split("=")
            arg_name = raw_arg[0]
            arg_value = raw_arg[1]
            if arg_name == "type":
                bot.init()
                if arg_value == "polling":
                    __start_polling(bot)
                elif arg_value == "webhook":
                    __start_webhook(bot)
            else:
                raise Exception("Requires arguments: --type")


def __start_webhook(bot: TelegramBot):
    start_webhook(
        dispatcher=bot.dispatcher,
        webhook_path=webhook_config.url.path,
        on_startup=bot.on_startup,
        skip_updates=True,
        host=webapp_config.host,
        port=webapp_config.port
    )


def __start_polling(bot: TelegramBot):
    executor.start_polling(bot.dispatcher, on_startup=bot.on_startup)


if __name__ == "__main__":
    main(sys.argv)


