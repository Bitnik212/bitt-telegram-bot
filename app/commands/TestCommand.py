from aiogram import Dispatcher, types

from core.Command import Command
from core.decorator.register_command import register_command


@register_command
class TestCommand(Command):

    def __init__(self, dispatcher: Dispatcher):
        super().__init__(dispatcher)
        self.name = "test"
        self.description = "test"
        self.chat_trigger_list = ["сенко, я дома"]

    async def handler(self, message: types.Message, **kwargs):
        return await message.answer("ssss")

    async def chat_message_handler(self, message: types.Message, **kwargs):

        return await message.reply("Добро пожаловать домой!")

