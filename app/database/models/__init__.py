from enum import Enum

from app.database import BaseModel
from app.database.models.TelegramStickerModel import TelegramStickerModel


class Models(Enum):

    @classmethod
    def list(cls) -> [BaseModel]:
        return [item.value for item in list(cls) if issubclass(item.value, BaseModel)]

    telegram_stickers = TelegramStickerModel


if __name__ == "__main__":
    print(Models.list())
