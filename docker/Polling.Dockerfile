FROM python:3.10
# set work directory
WORKDIR /application/
# copy project
COPY . /application/
# install dependencies
RUN pip install -r requirements.txt
# run app
CMD ["python", "main.py", "--type=polling"]
